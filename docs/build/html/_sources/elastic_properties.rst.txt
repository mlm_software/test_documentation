Elastic properties
=====================

.. _Elastic properties:


EntraPT is based on an internally-consistent database of elastic properties.
The database contains for each mineral phase the volume equation of state (EoS) and the 4th-rank elastic tensor at room conditions.
The elastic tensors are taken from experimental determinations reported in the literature, and, if necessary,
their components are rescaled to ensure that the Reuss bulk modulus equals that of the volume EoS at room conditions.
This ensures consistency throughout the entire calculation. During the rescaling, care is taken that the degree of anisotropy
(evaluated through the Universal anisotropic index, as defined in :cite:t:`Ranganathan2008`) is not altered (see :cite:t:`Mazzucchelli2019, mazzucchelli_entrapt_2021` for further details).
Only phases with published and validated elastic properties and EoS are included in the program database. 
Additional hosts and inclusion minerals will be added as more internally consistent elastic data and EoS become available.

Here are reported the elastic properties of the minerals that are included in the current database of EntraPT.
For each mineral, the equations of state (EoS) can be downloaded as files in EosFit format
(see `Ross Angel's website <http://www.rossangel.com/home.htm>`_ for further information on the EosFit program).

Diamond
*******************************
Our best estimate of the P-V-T EoS for diamond determined from a
critical review of the most recent experimental data (elasticity, heat capacity, PV data)
and DFT simulations :cite:p:`Angel2015diamond`.

The room conditions elastic constants (stiffness tensor) are taken from :cite:t:`Zouboulis1998`.
The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the P-V-T EoS for diamond of :cite:t:`Angel2015diamond`

+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 1078.4, C_{12} = 126.8`  |  :cite:t:`Zouboulis1998`       | | :cite:p:`Angel2015diamond`                                                                        |
| | :math:`C_{44} = 575.7`                   |                                | | `download EoS file <http://www.rossangel.com/Download/eos/diamond.eos>`_                          |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+

Garnet (new EoS)
*******************************

.. _Garnet (new EoS):


.. Attention::
	| **Use these EoS of garnet for host-inclusion calculations!**
	
	These EoS are identified in EntraPT with the suffix *_angel2022*
	
These PVT EoS are new determinations obtained by fitting all of the elasticity and volume data for these garnets
that is published in the literature. We first used published heat capacity data to determine which published data sets are consistent,
and then fitted the EoS parameters to all of those data together.
These are the most internally consistent EoS that it is possible to obtain with currently-available data,
and they should be used for all host-inclusion calculations. 
For further details on the EoS and their fitting see :cite:t:`angel_garnet_2022`.

The room conditions elastic constants (stiffness tensor) of almandine, grossular and pyrope are taken
from :cite:t:`Jiang2004, Isaak1992, Sinogeikin2002`, respectively.
The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the corresponding P-V-T EoS.

Almandine
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 303.7, C_{12} = 110.1`   |  :cite:t:`Jiang2004`           | | :cite:t:`angel_garnet_2022`                                                                       |
| | :math:`C_{44} = 94.4`                    |                                | | `download EoS file <http://www.rossangel.com/Download/eos/alm_qcompMGD.eos>`_                     |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Grossular
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 317.6, C_{12} = 91.7`    |  :cite:t:`Isaak1992`           | | :cite:t:`angel_garnet_2022`                                                                       |
| | :math:`C_{44} = 102.5`                   |                                | | `download EoS file <http://www.rossangel.com/Download/eos/gross_qcompMGD.eos>`_                   |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Pyrope
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 295.7, C_{12} = 106.2`   |  :cite:t:`Sinogeikin2002`      | | :cite:t:`angel_garnet_2022`                                                                       |
| | :math:`C_{44} = 93.5`                    |                                | | `download EoS file <http://www.rossangel.com/Download/eos/pyrope_qcompMGD.eos>`_                  |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+



Garnet (old EoS)
*******************************

.. warning::
	| **Do not use these EoS of garnet for host inclusion calculations!**
	
	These EoS are identified in EntraPT with the suffix *_milani2015* and *milani_2017* and should **not** be used for host-inclusion calculations.
	They are still provided as a choice in EntraPT only for comparison to the new garnet EoS given above. 
	
These are EoS for garnet determined before our ability to fit EoS parameters in EosFit to all available elasticity and volume data.
These EoS should not be used for host-inclusion calculations,
but are only provided for comparison to the new garnet EoS given above. The papers describing these old EoS are :cite:t:`Milani2015, Milani2017`.

The room conditions elastic constants (stiffness tensor) of almandine, grossular and pyrope are taken
from :cite:t:`Jiang2004, Isaak1992, Sinogeikin2002`, respectively.
The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the corresponding P-V-T EoS.

Almandine (old)
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 300.2, C_{12} = 108.8`   |  :cite:t:`Jiang2004`           | | :cite:t:`Milani2015`                                                                              |
| | :math:`C_{44} = 93.3`                    |                                | | `download EoS file <http://www.rossangel.com/Download/eos/almandine_garnet.eos>`_                 |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Grossular (old)
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 316.7, C_{12} = 91.5`    |  :cite:t:`Isaak1992`           | | :cite:t:`Milani2017`                                                                              |
| | :math:`C_{44} = 102.2`                   |                                | | `download EoS file <http://www.rossangel.com/Download/eos/grossular_garnet.eos>`_                 |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Pyrope (old)
---------------------------------
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions    | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+============================================+================================+=====================================================================================================+
| | :math:`C_{11} = 291.1, C_{12} = 100`     |  :cite:t:`Sinogeikin2002`      | | :cite:t:`Milani2015`                                                                              |
| | :math:`C_{44} = 93`                      |                                | | `download EoS file <http://www.rossangel.com/Download/eos/pyrope_garnet.eos>`_                    |
+--------------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Quartz
*******************************
This is a full P-V-T EoS for quartz, including the elastic softening in both alpha and beta quartz due to the alpha-beta phase transition,
and a curved alpha-beta phase boundary to fit all available reversals and constraints.
EoS parameters were determined by fitting simultaneously to both elastic moduli and P-T-V data.
For further details see :cite:t:`Angel2017`.

The room conditions elastic constants (stiffness tensor) are taken from :cite:t:`Lakshtanov2007`.
The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the P-V-T EoS for quartz of :cite:t:`Angel2017`

+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+=========================================+================================+=====================================================================================================+
| | :math:`C_{11} = 86.1, C_{12} = 7.2`   |  :cite:t:`Lakshtanov2007`      | | :cite:t:`Angel2017`                                                                               |
| | :math:`C_{13} = 11.7, C_{14} = 17.7`  |                                | | `download EoS file <http://www.rossangel.com/Download/eos/quartz_angeletal2017_fullcurved.eos>`_  |
| | :math:`C_{33} = 105.6, C_{44} = 59.2` |                                |                                                                                                     |               
+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+

..
	Olivine
	*******************************
	Our best estimate of the P-V-T EoS for mantle composition olivine (Fo90-Fo92) determined from a critical review and a combined fit of elasticity and
	volume data from single-crystal measurements. 
	For further details see :cite:t:`Angel2018`.

	The room conditions elastic constants (stiffness tensor) are taken from :cite:t:`Abramson1997`.
	The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the P-V-T EoS for quartz of :cite:t:`Angel2018`

	+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
	| :math:`C_{ij}` (GPa) at room conditions | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
	+=========================================+================================+=====================================================================================================+
	| | :math:`C_{11} = 317.8, C_{12} = 67.5` |  :cite:t:`Abramson1997`        | | :cite:t:`Angel2018`                                                                               |
	| | :math:`C_{13} = 71.0, C_{22} = 194.8` |                                | | `download EoS file <http://www.rossangel.com/Download/eos/mantleolivine_BM3_MGD.eos>`_            |
	| | :math:`C_{23} = 76.2, C_{33} = 231.5` |                                |                                                                                                     |               
	| | :math:`C_{44} = 63.5, C_{55} = 76.4`  |                                |                                                                                                     |
	| | :math:`C_{66} = 78.0`                 |                                |                                                                                                     |
	+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+


Zircon
*******************************
Isothermal-type EoS for the zircon volume and unit-cell parameters fitted to new PV and elasticity data for zircon, together with literature TV data.
This EoS has been developed by Alix Ehlers of Virginia Tech.
For further details see :cite:t:`ehlers_thermoelastic_2022`.

The room conditions elastic constants (stiffness tensor) of a nonmetamict zircon are taken from :cite:t:`Ozkan1974`.
The moduli are rescaled on the room conditions Reuss bulk modulus (KTR0) of the P-V-T EoS for quartz of :cite:t:`ehlers_thermoelastic_2022`

+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+
| :math:`C_{ij}` (GPa) at room conditions | :math:`C_{ij}` modified after: | EoS from:                                                                                           |
+=========================================+================================+=====================================================================================================+
| | :math:`C_{11} = 422.0, C_{12} = 70.0` |  :cite:t:`Ozkan1974`           | | :cite:t:`ehlers_thermoelastic_2022`                                                               |
| | :math:`C_{13} = 148.9, C_{33} = 488.0`|                                | | `download EoS file <http://www.rossangel.com/Download/eos/zircon_acV_BM3_isothermal.eos>`_        |
| | :math:`C_{44} = 113.1, C_{66} = 48.3` |                                |                                                                                                     |               
+-----------------------------------------+--------------------------------+-----------------------------------------------------------------------------------------------------+

