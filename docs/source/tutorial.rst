Tutorial
==================
In this tutorial you will learn how EntraPT can be used in practice to: 

* interpret the residual strain measured in several inclusions
* calculate the residual pressure and the entrapment conditions evaluating the uncertainties for each step of the calculation
* store all of the data in a consistent way and use them for further analysis


In this example we will use on the data published by :cite:t:`bonazzi_assessment_2019`,
obtained from hydrothermal synthesis experiments with a piston-cylinder press to produce quartz inclusions
in pure almandine garnet (>99%) at eclogite facies metamorphic conditions.
In the study two experiments were performed and labelled Alm-1 (synthesis performed at P = 3.0 GPa and T= 775 °C)
and Alm-2 (at P= 2.5 GPa and T= 800 °C). From each experiment they recovered several host crystals,
each containing one or more crystals of quartz as inclusions. Isolated, fully-enclosed quartz inclusions in the recovered garnets
were then investigated using micro-Raman spectroscopy.

The changes in the Raman peak positions were measured at the central
point of the inclusion and interpreted by applying the phonon-mode Grüneisen tensors
of quartz :cite:p:`murri_raman_2018` to obtain the full strain state of each inclusion,
using the program stRAinMAN :cite:p:`Angel2018Strainman`.
From the residual strain they calculated the full residual anisotropic stress state and the mean stress
by using the elastic properties of quartz. The authors showed that the entrapment pressures
calculated from this mean stress with the isotropic model for host-inclusion systems differ from
the known synthesis pressure by <0.2 GPa, which is on the order of the combined experimental uncertainties.
Their results show that the most significant effect of the elastic anisotropy of quartz is on the Raman shifts of the inclusion,
and not on the subsequent calculation of entrapment conditions.

.. _files for tutorial bonazzi:

.. note::
	Download `here <https://www.mineralogylab.com/wp-content/uploads/2022/09/bonazzi_et_al_2019_v1.4.0.zip>`_ 
	the compressed folder containing the input file, the project file and the MATLAB® script that will be used in this tutorial.

Add new analyses: set the host-inclusion system and import the measured residual strains
******************************************************************************************
The first step is to set the host and the inclusion minerals and to import your
*analyses* into EntraPT (you can learn here what an :ref:`analysis<Definition of an *analysis* in EntraPT>` is in EntraPT).

The new analyses are added from the *Add New Analyses* window (:ref:`set system bonazzi`-a). 
This window has a panel on the right to navigate through the *Host&Inclusion* 
(:ref:`set system bonazzi`-b)
and *Strain* pages (:ref:`set system bonazzi`-c),
where all of the parameters that define an analysis can be set.

.. _set system bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Set_HostInc_mod.jpg
  :width: 800
  :alt: Set the host and inclusion minerals
  
  Fig. 1

Since all of the measurements refer to quartz inclusions in almandine,
you need to set select *almandine_milani2015* as the host and *quartz* as the inclusion from
the drop down list in the *Host&Inclusion* page (:ref:`set system bonazzi`-d,e).
Confirm your choice with the *Confirm and Set Strain* button (:ref:`set system bonazzi`-f). This will lead you to the next page.

.. Attention::
	The calculations performed in :cite:t:`bonazzi_assessment_2019` used the equation of state (EoS) for almandine
	labeled as *almandine_milani2015*. Since then, newer EoS for garnet end-members have become
	available (from :cite:t:`angel_garnet_2022`).
	We recommend that you use the newer EoS labeled as *angel2022* for your calculations other than this tutorial.
	See :ref:`here what has changed<Garnet (new EoS)>`.


.. _set strain bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Import_multiple_analysis_mod_2.jpg
  :width: 800
  :alt: Set the host and inclusion minerals
  
  Fig. 2
  
In the *Strain* page (:ref:`set strain bonazzi`-a) you will set the strains of the quartz inclusions in almandine.
Since all the measurements belongs to the same host-inclusion system with the same
mineral phases, you can choose to import the strains for multiple analyses.
Click on the *Multiple Analyses* (:ref:`set strain bonazzi`-b) option and
then on *Import File* (:ref:`set strain bonazzi`-c).
A file-dialog is opened from which you choose an input file (with \*.dat extension) from your computer.
Select the \*.dat file that you have downloaded from :ref:`here<files for tutorial bonazzi>`,
which contains the data of the ideal inclusions from the Tables 2a and 2b of :cite:t:`bonazzi_assessment_2019`.

.. Note::

	Since the inclusions (quartz) are uniaxial,
	the values :math:`𝜀_1 = 𝜀_2` and :math:`𝜀_3` together with their esd and covariance are given in the 
	\*.dat file formatted for uniaxial inclusions. Input files formatted for different symmetries are also available.
	See here a description of the :ref:`format of these files<Structure of the input file for importing the strains>`.

When you confirm, the input file is loaded to EntraPT and the 
:ref:`consistency check<Import the strains from a file>` is performed.
A pop-up window will show a summary of the analyses that have passed the checks and those that have not.
The data of the analyses that passed all the checks are displayed in a table (:ref:`set strain bonazzi`-d).

Click *Add analyses to workspace* (:ref:`set strain bonazzi`-e) to add
these analyses to the *Workspace* (:ref:`set strain bonazzi`-f). 
The analyses are now stored in the current project,
and can be selected from the :ref:`workspace<The Workspace>`
for calculations
or to show their data in plots.


Visual analysis of the residual strain
*********************************************
Once the measured strains are stored in the project,
the user can at any time perform a visual analysis of the
residual strain from the *Plot Strain* page (:ref:`plot strain bonazzi`-b)
in the *View Data* window (:ref:`plot strain bonazzi`-a) .

.. _plot strain bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Plot_strain_mod.jpg
  :width: 800
  :alt: Set the host and inclusion minerals
  
  Fig. 3
  
Select all the analyses from the *Workspace* (:ref:`plot strain bonazzi`-c)
(:ref:`see here how<The Workspace>`) to show
their residual strains in a plot of :math:`𝜀_1` vs :math:`𝜀_3`.
The error bars and confidence ellipses obtained from the esd and covariances on the
residual strain are also displayed.

You can choose for which of the selected analyses the labels and the confidence ellipses are shown,
from the panel shown in Fig. :ref:`plot strain bonazzi` - d.
Click on *Refresh Plot* to update the plot and see the changes.

The isochors and the lines of isotropic strain and
hydrostatic stress can be also added to the plot, 
by selecting them from the panel of :ref:`plot strain bonazzi`-e.
Click on *Refresh Plot* to update the plot.
The same plot as Fig. 5a of :cite:t:`bonazzi_assessment_2019` will be
displayed with the addition of the confidence ellipses of each analysis.

You can change the variables shown on the x and y axes (Fig. :ref:`plot strain bonazzi` - f,g).
For some symmetries of the inclusion, some of these choices are equivalent
(for example, for uniaxial inclusions :math:`ε_1 = ε_2`). 
Also in this case, click on *Refresh Plot* to update the plot and see the changes.
Note that not all the combinations are currently supported. 

Use the *Plot settings* (Fig. :ref:`plot strain bonazzi`-h) to
:ref:`set the P and T range<Settings of plots>` of the axes.

The buttons in the toolbar of the plot
(:ref:`plot strain bonazzi`-i) allow the user to select any point on the plot to
:ref:`get its coordinates <Get the cursor position in a plot>`,
and to zoom in and out.
From here, the plot can be directly :ref:`exported as a picture <Export the plots as pictures>`
in several formats.
 
You can also filter the list on analyses displayed in the
*Workspace*. As an example, by typing "Alm1" in the
:ref:`search tool<Search analyses>` (:ref:`plot strain bonazzi`-j),
you can filter the analyses of the first experiment
of :cite:t:`bonazzi_assessment_2019`.

Calculate the entrapment isomekes
*********************************************
The entrapment conditions are calculated from the *Calculate Entrapment* window
(:ref:`calculate isomeke bonazzi`-a).
Select one or more analyses from the Workspace (:ref:`see here how<The Workspace>`)
(:ref:`calculate isomeke bonazzi`-b) to calculate
their :ref:`entrapment isomekes<The isomeke>`.


.. _calculate isomeke bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Calculate_entrapment_mod.jpg
  :width: 800
  :alt: Set the host and inclusion minerals
  
  Fig. 4
  


You can set the range of
temperatures for the calculation of the entrapment isomekes using the
Tmin, Tmax and Tstep fields in this window (:ref:`calculate isomeke bonazzi`-c).
Initially, the units for temperature (Tscale) and for the pressure
(Pscale) are set to °C and GPa respectively, but other options are K and kbar,
respectively. See here how to
:ref:`change the pressure and temperature units<Change the P and T scales>`.

The inclusions in the dataset that we are using for this tutorial were synthesized
by :cite:t:`bonazzi_assessment_2019` in two experiments conducted at 3 GPa, 775 °C
(Alm1) and 2.5 GPa, 800 °C (Alm2). Therefore, setting Tmin = 750 °C,
Tmax = 850 °C, Tstep = 5 °C as the temperature range for the calculation of the entrapment isomeke
is a good choice for the inclusions of both experiments.
The final conditions at which the residual strain was measured are always assumed to be room
conditions (T = 25 °C or 298 K, P = 0 GPa or 0 kbar) (:ref:`calculate isomeke bonazzi`-d).

:cite:t:`bonazzi_assessment_2019` calculated the residual pressure from the measured residual
strain by two different approaches. In the first approach the residual stress
is obtained from the strain applying the elastic stiffness tensor (:math:`C_{ij}`) of the inclusion.
In the second approach, the equation of state (EoS) of the inclusion
is used to obtain the residual pressure from the residual volume strain of the inclusion.
EntraPT implements
:ref:`both these two approaches<How EntraPT calculates the residual stress and pressure from the strain>`.
You can choose one or both the approaches from the *Expert Mode*
panel (:ref:`calculate isomeke bonazzi`-e).
By default, the *Expert Mode* panel is not active, but can be activate from the
*Settings* menu in the top bar of EntraPT.
The chosen residual pressure(s)
will be used for the calculation of the entrapment isomeke.

For the current example, we will calculate
the residual pressure using both methods (:ref:`calculate isomeke bonazzi`-e). Select all the analyses
from the Workspace, and click *Calculate* (:ref:`calculate isomeke bonazzi`-f).
The calculation of the entrapment isomeke of all the analsyses is run at once with the
same parameters (Tend, Tmax, Tstep).
Once the calculation is completed, the results are saved to the current project
and can be viewed from the :ref:`View Data` window.

You can also calculate the entrapment
isomekes for each analysis independently by selecting one at time from the *Workspace* and
setting the appropriate calculation parameters.

Read here further details on the  
:ref:`calculation of the entrapment isomekes<Calculate the entapment conditions>`.

View and plot the results
*********************************************
The results from each analysis can be viewed from the *View Data* window
(:ref:`show results bonazzi`-a).

.. _show results bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Plot_isomekes_mod.jpg
  :width: 800
  :alt: Set the host and inclusion minerals
  
  Fig. 5
  
By selecting one analysis from the *Workspace*,
the *Details* page (:ref:`show results bonazzi`-b)
shows all of the details of the selected analysis such as the label,
the notes, the host-inclusion system, and the residual strain.
The *Results* page (:ref:`show results bonazzi`-c)
shows all of the numerical results of the calculations for
the selected analysis
(residual pressure determined with each model, the P-T points on the isomekes,
and all of the uncertainties).

The *Plot Isomekes* page (:ref:`show results bonazzi`-d)
shows a P-T graph reporting the isomekes calculated for each analysis
(:ref:`show results bonazzi`-e).
Select one analysis from the *Workspace* to plot the isomeke(s)
obtained using the models
selected for the calculation, with their estimated uncertainties shown as a shaded area.
The :ref:`uncertainties on the isomekes<The isomeke>` are estimated assuming an uncertainty
equal to one standard deviation on the residual pressure.

You can selectively hide or show one or more objects of the plot
(isomekes, shaded area of the uncertainty, legend, labels) from the
panel in :ref:`show results bonazzi`-f,g. 
Remember to click on *Refresh Plot* to update the plot.

Use the *Plot settings* (:ref:`show results bonazzi`-h) to
:ref:`set the P and T range<Settings of plots>` of the axes.
The buttons in the toolbar of the plot
(:ref:`show results bonazzi`-i) allow the user to select any point on the plot to
:ref:`get its coordinates <Get the cursor position in a plot>`,
and to zoom in and out.
From here, the plot can be directly :ref:`exported as a picture <Export the plots as pictures>`
in several formats.

The user can also generate a plot with the isomekes of multiple analyses by selecting
two or more from the Workspace.
Specific analyses or groups of analyses can be filter by using the 
:ref:`search tool<Search analyses>` below the *Workspace*
(:ref:`show results bonazzi`-j).

Export project: save data to your computer and further processing
*************************************************************************

You can export all the data of the current project
as a project file from the *File -> Export Project* menu
in the to bar of EntraPT.
Project files can be imported back into EntraPT,
using the *File -> Import Project* menu, to view the data, and generate new plots. 
Read more about :ref:`project files<Export and import project files>` here.

The \*.ept project file can be opened and processed using MATLAB®.
Short scripts can be implemented to rapidly produce custom plots that
are not directly displayed in EntraPT, taking advantage that all of the data
are structured consistently in the project file.

.. _entrapment pressure bonazzi:

.. figure:: images/screenshots/tutorial_bonazzi/Ptrap_vs_diffstress.jpg
  :width: 400
  :alt: Set the host and inclusion minerals
  
  Fig. 6

An example is the plot of the entrapment pressures as a function of the residual
differential stress (:ref:`entrapment pressure bonazzi`).
It has been generated externally with the MATLAB® script provided :ref:`here<files for tutorial bonazzi>`,
using the data contained in the project file created with this tutorial. 

During the export procedure from EntraPT, the user can also choose to save the data to
spreadsheets (with \*.xlsx extension) that can be read by any commonly used spreadsheet
application, such as Microsoft Excel® or LibreOffice®.
In this case an individual spreadsheet is created for each analysis.
A compressed folder, with \*.zip extension, is created that contains the \*.ept project
file and one or more folders containing the spreadsheet files.
  