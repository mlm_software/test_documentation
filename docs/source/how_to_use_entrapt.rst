How to use EntraPT
==================

Definition of an *analysis* in EntraPT
****************************************
EntraPT works on analyses. Each analysis is a container of all of the information relative to one measurement
of residual strain and the calculations performed on it. An analysis is defined at least by:

* A label
* A pair of mineral phases: the host and the inclusion phase
* The elastic properties of the host and the inclusion (EoS, stiffness tensors)
* The geometry of the host-inclusion system
* The relative orientation between the host and the inclusion
* The residual strain state defined by the components of residual strain vector 

Other data that can be optionally associated with an analysis are:

* The labels of the sample, thin section, host, inclusion and of the point analysis
* A text description of each analysis
* The covariance matrix associated to the residual strain
* The results of the calculation of its entrapment conditions (i.e. P and T of the entrapment isomeke, and all the intermediate results, such as residual stress, unrelaxed strain, their covariance matrices, etc..)

With this definition, two measurements of residual strain taken in the same inclusion crystal are stored
and treated as separate analyses in EntraPT. In fact, even if they belong to the same host-inclusion system 
(same mineral phases, geometry and orientation) they can differ in the residual strain.
For the same reason, measurements from several inclusions in the same host crystal are also considered as separate analyses.
By setting custom labels, the user has the freedom to give the appropriate labels to each analysis,
for example in order to identify analyses that belong to the same inclusion or to the same host.
This allows for great flexibility in storing and processing the measurements, since each analysis can have different data and metadata and can be handled separately.



Add new analyses to the current project
*******************************************
In EntraPT the user first needs to add one or more new analyses to the project from the *Add New Analyses* tab.


  


Set the host and the inclusion minerals
##########################################
Since the calculation of the entrapment conditions requires the elastic properties of the minerals
(i.e. the pressure-volume-temperature equations of state, and the elastic tensors)
EntraPT is based on an internally-consistent database of elastic properties (see section :ref:`Elastic properties`).


The first step in order to add anew analysis, is to select the host and the inclusion minerals.
On the right panel choose the option *Host&Inclusion*. 
Here you can select the host and the inclusion minerals, whose property will be used for the calculations (Fig. :ref:`set system`).
Once the minerals are selected, a summary of their properties will appear in this window.
Click on the button *Confirm and Set strain* at the bottom of the window to confirm the selected minerals and jump to the next step.

.. _set system:

.. figure:: images/screenshots/Set_host_inclusion.jpg
  :width: 800
  :alt: Add new analyses to EntraPT
  
  Set the host and inclusion minerals
  

	
.. Attention::
	All the strains, labels and notes that you will add in the next steps will assume that the host and the inclusion
	minerals are those currently selected in the "Host&Inclusion" window.
	If you want to change the host and/or the inclusion
	please refer to the section :ref:`Change the host and/or the inclusion minerals`


Set the residual strain of the inclusion
############################################
Once the properties of the system are set, the user must input the residual
strain measured in the inclusion for each analysis.

Since the esd and the covariances associated with the residual strain are needed to propagate the uncertainties into the residual
pressure and the entrapment isomeke, the user can input also this information for each analysis.

The residual strain can be measured with Raman spectroscopy or X-ray diffraction (see the section :ref:`Residual strain and stress`).
For inclusions such as quartz, zircon, rutile the strain state can be obtained from the measured Raman shifts by using the program stRAinMAN
(freely available at `Ross Angel's website <http://www.rossangel.com/home.htm>`_). The program stRAinMAN allows the user to import the measured Raman shifts and to export
the resulting strain components and the associated standard deviations and correlations in a logfile.
For a uniaxial inclusion without symmetry breaking,
stRAinMAN provides an output with the sum of the strain components (in Voigt notation)
:math:`ε_1+ε_2`, the component :math:`ε_3`, their estimated standard deviations (esd)
and the covariance between :math:`ε_1+ε_2` and :math:`ε_3`. 


Set the strains in the GUI
---------------------------------

The user can input directly into EntraPT the components of the residual strain and the associated uncertainties for each analysis (Fig. :ref:`set the strains`). 

While you are in the *Add new analyses* tab, select the option *Strain* from the panel on the right.
In this window, choose *Single analysis*. Here you can set the components of the strain and the components of the covariance matrix.
Depending on the symmetry of the inclusion, some components are equivalent by symmetry. Therefore, only some of the fields might be editable. 
The other fields will be updated automatically according to the symmetry of the system.
 
Note that the values are not rescaled, which means that all the values must have the appropriate number of decimal digits.
The scientific notation is allowed, e.g. -0.001 can be written 
as -1e-3.
You can also give a label and a text note that will be associated to this analysis.

.. _set the strains:

.. figure:: images/screenshots/Set_strain_in_GUI.jpg
  :width: 800
  :alt: Set the strain components of the inclusion
  
  Set the strain components of the inclusion

Once the components are set, click the button *Add analysis to workspace*.
You will see a new analysis appearing in the *Workspace* area on the left side of the app.
You can now repeat the procedure and input the strain components, label and note of a new analysis. This will add a new analysis to the workspace.

.. Attention::
	All the analysis that you add with this procedure assume that the host and the inclusion minerals
	are those currently selected in the *Host&Inclusion* window. If you want to change the host and/or the inclusion
	please refer to the section :ref:`Change the host and/or the inclusion minerals`





Import the strains from a file
---------------------------------
The application of elastic geobarometry usually requires to measure the strain of several inclusions.
EntraPT allows the he user to import a file with a list of analyses with their strains and the associated uncertainties (esd’s and covariances).
When you import the analysis from a file, EntraPT assumes that the host and the inclusion minerals are those currently selected in the 
"Host&Inclusion" window.

The input file must have a \*.dat extension, and be a tab-delimited text file (see :ref:`Structure of the input file for importing the strains` )


To import the input file go to the *Strain* window and select *Multiple Analyses* (See Fig. :ref:`import the strains` ).
This will open a new panel. Here click on the button *Import file*. A window will open. You can directly drop the \*.dat file in this window
or open a file browser.

While importing the data, EntraPT checks that the file is readable and consistent.
Depending on the symmetry of the selected inclusion,
a minimum number of independent strain components is required. Moreover, the symmetry of
the inclusion prescribes the equality between some of the components of the strain.
A check is also performed on the consistency of the provided esd and covariances,
since the resulting covariance matrix must be positive definite.
Analyses that do not satisfy these requirements are discarded during the import process, and a
detailed message describing the errors is shown to the user.
A pop-up window will show a summary of the analyses that have passed the checks and those that did not.

The data of the analyses that passed all the checks are displayed in a table. The user can now add these analyses to the workspace
by clicking on the button *Add analyses to workspace*.
The labels of the imported analyses will appear in the *Workspace* on the left side of the app, and they can now be selected for calculations.

.. note::
	The analyses are not added to the workspace and cannot be used for calculations until the user clicks on the button *Add analyses to workspace*.



.. _import the strains:

.. figure:: images/screenshots/Import_multiple_analysis_2.jpg
  :width: 800
  :alt: Add analyses to workspace
  
  Add analyses to workspace



Structure of the input file for importing the strains
-----------------------------------------------------
.. _Structure of the input file for importing the strains:


.. note::
	Find `here <https://www.mineralogylab.com/templates_dat_files/>`_ the template files for importing strain data into EntraPT.
	The header of the files must not be modified or EntraPT will not recognise properly the content of the columns.


The input file must have a \*.dat extension, and be a tab-delimited text file.	
The template file can be edited with a spreadsheet editor (for example Microsoft Excel®, LibreOffice®, etc),
to preserve the tab delimited structure.
While saving the file, **be sure that your editor saves it as a tab delimited file**.

The general structure of all the input file is as follow:

* You can reserve as many lines as you want for comments before the header.
* The content of each column is specified by the header.
* The header must not be modified.
* The format of the header depends on the symmetry of the inclusion. Be sure to download the correct template file.
* Each line after the header contains the data of one analysis.
* All of the analyses in one \*.dat file must belong to the same host-inclusion system (i.e.
  they must have the same host and inclusion minerals).
* Five columns are reserved for the labels to identify the analysis
  (sample, thin section, host, inclusion and point analysis) and one column for the notes associated to the analysis.
* The content of the remaining columns depends on the symmetry of the residual strain,
  which corresponds to the symmetry of the inclusion in absence of symmetry breaking.
* If you do not have the uncertainties (esd and covariances) on the residual strain components, set the values of the corresponding columns to 0.											
  Do not leave those columns empty, since rows with empty strain components and/or esd and/or covariances are not imported.	
* You can also reserve one or more rows for comments to separate groups of data. The rows reserved for comments are not imported.								
* When saving the file, be sure that your editor saves it as a tab delimited format file
* After saving, check that the number of decimal digits in the file are preserved.
  Depending on the setting of you editor, numbers might be rounded.
  This could cause inconsistencies in the values, especially for uncertainties.
  Usually EntraPT will detect these inconsistencies when importing the data, since they often results
  in covariance matrices that are not positive definite.


**Uniaxial inclusion (tetragonal, trigonal, hexagonal)**: 
If the inclusion is uniaxial without symmetry breaking both the strain and the covariance matrix have fewer independent components.
For inclusions such as quartz, zircon, rutile the strain state can be obtained from the measured Raman shifts by using the program stRAinMAN
(freely available at `Ross Angel's website <http://www.rossangel.com/home.htm>`_).
The program stRAinMAN allows the user to import the measured Raman shifts and to export
the resulting strain components and the associated standard deviations and correlations in a logfile.
A specific template is provided to import the data obtained from stRAinMAN into EntraPT (Fig. :ref:`template uniaxial`). The measured strain components and the statistical parameters
(esd, covariances) can be easily copied and pasted from the stRAinMAN logfile to this input file using any spreadsheet editor.

The strain and the uncertainties of uniaxial inclusions can also be obtained from X-ray diffraction measurements.
Depending on the format of the output data obtained
from diffraction measurements the :ref:`general template<Inclusion with lower symmetry or with symmetry breaking>`
might be more appropriate.

.. _template uniaxial:

.. figure:: images/screenshots/Template_uniaxial.jpg
  :width: 800
  :alt: Template for uniaxial inclusions
  
  Template for uniaxial inclusions
  
**Cubic inclusion**:
When the inclusion is cubic
(without symmetry breaking), the volume strain is enough to describe the strain state of the inclusion. Therefore, the volume strain and
its esd can be specified in the appropriate input file for cubic inclusions.

.. _template cubic:

.. figure:: images/screenshots/Template_cubic.jpg
  :width: 800
  :alt: Template for cubic inclusions
  
  Template for cubic inclusions


.. _Inclusion with lower symmetry or with symmetry breaking:

**Inclusion with lower symmetry or with symmetry breaking**: 
a general-purpose file, suitable for any symmetry of the strain, is also provided that contains one column for each strain component (6 columns), and one column for each independent entry of the
covariance matrix (21 columns).


Change the host and/or the inclusion minerals
####################################################


You can change the host and/or the inclusion mineral from the *Host&Inclusion* window (Fig. :ref:`set system`). 
Click on the button *Change H-I system* at the bottom of this window and make a new selection.
Once the new minerals are selected, a summary of their properties will appear in this window.
Click on the button *Confirm and Set strain* at the bottom of the window to confirm the selected minerals.
Now you can go to the *Strain* tab and add one or more new analyses that will refer to the currently selected host and
inclusion minerals.

The Workspace
*********************************
The *Workspace*, on the left of the app, shows the labels of all the analyses in the current project.

The analyses are grouped in host-inclusion pairs depending on their host and inclusion mineral phases.
Each group can be collapsed or expanded by clicking on the small arrow at the left of the group name.

Whenever you want to perform an operation on one analysis (e.g. calculate its entrapment isomeke, or plot its strain)
you have to select it from the *Workspace*. In some cases (e.g. for plotting multiple
analyses in the same figure) you can select several analyses at the same time.
To select several analyses, press the shift button of your keyword while
you select the analyses with your mouse (very much like you would do in order to 
select several files from a folder on your computer).



Search analyses
#################################
You can narrow down the list of analyses displayed in the *Workspace* by using the search tool.

Type a string in the search box below the *Workspace* and click on the search button (on the left of the search box).
Only the analyses that contain the typed string in their labels or notes will now appear in the *Workspace*.
To clear the search, click on the button on the right side of the search box.

Delete analyses
#################################
You can delete one or more analyses from the current project.

Select the analyses from the *Workspace* and click the *Delete Selected Analyses* button at the bottom of the *Workspace*

.. Attention::
	This operation cannot be undone. Once you confirm, the selected analyses are deleted from the project file and cannot be recovered.

Calculate the entapment conditions
*******************************************
The entrapment conditions (i.e. the entrapment isomekes) are calculated from the *Calculate Entrapment* window
(Fig. :ref:`Calculate entrapment conditions` - a).
You can read :ref:`here<The isomeke>` an explanation of the concept of entrapment isomeke and how EntraPT calculates
the isomekes and their uncertainties.


.. _Calculate entrapment conditions:
.. figure:: images/screenshots/Calculate_entrapment_mod.jpg
  :width: 800
  :alt: Calculate the entrapment conditions
  
  Calculate the entrapment conditions
  
In this window you can set the range of temperatures for the calculation of the entrapment isomeke using the Tmin, Tmax and Tstep fields
(Fig. :ref:`Calculate entrapment conditions` - b).
By default, the units for temperature (Tscale) and for the pressure (Pscale) are set to °C and GPa respectively,
but they can be set to K and kbar, respectively, from the Settings menu.
The calculation always assumes that final pressure and temperature at which the residual strain was measured are the ambient conditions
(298 K, 0 GPa) (Fig. :ref:`Calculate entrapment conditions` - c).

The *Expert Mode* panel (Fig. :ref:`Calculate entrapment conditions` - d) can be enabled from the *Settings -> Expert mode* 
menu in the top bar of the application.
In the *Expert Mode* panel you can choose one or both of the two available models to convert the residual strain into stress.
Read :ref:`here<How EntraPT calculates the residual stress and pressure from the strain>` a description of these models. 
The chosen residual pressure(s) will be used for the calculation of the entrapment isomeke. If both models of residual pressure
are selected, two isomekes will be calculated for each analysis.

You have to select at least one analysis from the *Workspace* before you start a calculation (Fig. :ref:`Calculate entrapment conditions` - e).
By pressing the *Calculate* button at the bottom of the window (Fig. :ref:`Calculate entrapment conditions` - f),
the calculation of the entrapment isomeke(s) for the selected analysis will start.
You can select more than one analysis from the *Workspace* (:ref:`see here how<The Workspace>`).
In this way you will calculate the isomekes of all the selected analyses in a single run.
For all the analyses, the current settings that you have chosen in the *Calculate Entrapment* window will be uesd.

While the calculation is in progress, a wait bar will be displayed showing
the progress in the calculation. You can interrupt the calculation at any moment by pressing on the *Cancel* button. 
The process will be interrupted at the end of the calculation of the current isomeke.

Once the calculation is terminated, the results are stored in the current project and can be viewed from the :ref:`View Data` window.


View Data
*********************
In the *View Data* window (Fig. :ref:`Fig plot the isomekes` - a) you can view all of the data relative to each analysis and generate the plots.

Details of one analysis
#################################
The details are shown in the *Details* page
(Fig. :ref:`Fig plot the isomekes` - b).
Select one analysis from the *Workspace* to see its label, notes, the host and inclusion minerals and its residual strain state.


Results of the calculation
#################################
The results of the calculation of the entrapment isomeke(s) are shown in the *Results* page
(Fig. :ref:`Fig plot the isomekes` - c).

Select one analysis from the *Workspace* to see its results.

In this page a summary of the calculation is reported, that included the warning/error messages issued during the calculation.
The residual pressure calculated with the selected model(s) is reported with its uncertainties.
The pressure-temperature points along the calculated isomeke(s) are also shown.


Plot the isomekes
#################################
The entrapment isomekes are shown in a pressure-temperature plot under the *Plot Isomekes* page
(Fig. :ref:`Fig plot the isomekes` - d).

.. _Fig plot the isomekes:
.. figure:: images/screenshots/Plot_isomekes_mod.jpg
  :width: 800
  :alt: View data and results
  
  View data and results

Select one analysis from the *Workspace* to see the plot of its isomeke(s).
One or two isomekes are displayed for each analysis, depending on the model to convert strain to pressure during the calculation
(Fig. :ref:`Fig plot the isomekes` - e)
(:ref:`read more here<Calculate the entapment conditions>`).
If the uncertainties on the residual strain were provided, the uncertainties on the entrapment isomekes will be displayed as
a shaded area.

You can choose which of the calculated isomekes and associated uncertainties are displayed in the plot
from the panel shown in Fig. :ref:`Fig plot the isomekes` - f.
Select the items that you want to appear in the plot and click on *Refresh Plot* to update the plot.

You can plot the isomekes of several analyses in the same plot, if they were calculated using the same
pressure and temperature units.
Select multiple analyses from the *Workspace* (:ref:`see here how<The Workspace>`).
All their isomekes will appear in the plot.
This is particulary useful to check the possible intersection between isomekes of different inclusion phases measured in the
same host. For example the intersection between the isomekes of quartz and zircon inclusions included in
the same garnet (supposed to be included at the same conditions) has been suggested as way to constrain the
entrapment conditions of the system by :cite:t:`zhong_analytical_2019`.
From the panel shown in Fig. :ref:`Fig plot the isomekes` - g, choose the analysis whose labels you want to be displayed in the plot.

Use the *Plot settings* (Fig. :ref:`Fig plot the isomekes`-h) to
:ref:`set the P and T range<Settings of plots>` of the axes.
The buttons in the toolbar of the plot
(:ref:`Fig plot the isomekes`-i) allow the user to select any point on the plot to
:ref:`get its coordinates <Get the cursor position in a plot>`,
and to zoom in and out.
From here, the plot can be directly :ref:`exported as a picture <Export the plots as pictures>`
in several formats.


Plot the residual strain
#################################

The residual strain of one or more analyses can 
be analyzed visually in a scatter plot from the *Plot Strain* page
(Fig. :ref:`Plot of residual strain` - b)
under the *View Data* window (Fig. :ref:`Plot of residual strain` - a).

.. _Plot of residual strain:
.. figure:: images/screenshots/Plot_strain_mod.jpg
  :width: 800
  :alt: Plot of residual strain
  
  Plot of residual strain

Select one or more analysis from the *Workspace* (Fig. :ref:`Plot of residual strain` - c).
To select several analyses at the same time, press the shift button of your keyword while
selecting the analyses with your mouse (very much like you would do in order to select several files from a folder on your computer).
Remember that you can filter the list of analyses shown in the *Workspace*
by :ref:`performing a search<Search analyses>`.

For each of the selected analyses, the associated confidence 
ellipse and error bars are shown, as obtained from the variance-covariance matrix determined from the measurement of the residual strain.

You can choose for which of the selected analyses the labels and the confidence ellipses are shown,
from the panel shown in Fig. :ref:`Plot of residual strain` - d.
Click on *Refresh Plot* to update the plot and see the changes.

For a visual reference, the isochors and the lines of isotropic strain and hydrostatic stress state of the inclusion can be also added to the plot.
In this case, you must select from the *Workspace* only analyses that have the same mineral phase as inclusion.
Select the elements (lines and legend) that you want to add to the plot from the panel shown in Fig. :ref:`Plot of residual strain` - e. 
Then click on *Refresh Plot* to update the plot.

You can change the variables shown on the x and y axes (Fig. :ref:`Plot of residual strain` - f,g).
For some symmetries of the inclusion, some of these choices are equivalent (for example, for uniaxial inclusions :math:`ε_1 = ε_2`). 
Also in this case, click on *Refresh Plot* to update the plot and see the changes. Note that not all the combinations are currently supported. 

Use the *Plot settings* (Fig. :ref:`Plot of residual strain`-h) to
:ref:`set the P and T range<Settings of plots>` of the axes.

The buttons in the toolbar of the plot
(:ref:`Plot of residual strain`-i) allow the user to select any point on the plot to
:ref:`get its coordinates <Get the cursor position in a plot>`,
and to zoom in and out.
From here, the plot can be directly :ref:`exported as a picture <Export the plots as pictures>`
in several formats.


Get the cursor position in a plot
###################################
You can get the coordinates of your mouse cursor in a plot.
This is useful when you need the value of an element displayed in
a plot (e.g. the strain state of an inclusion displayed as a point in a strain plot).

By moving your mouse on the top-right side of each plot, a pop-up bar will be displayed 
(see for example Fig. :ref:`Fig plot the isomekes` - i).
Click on the first button from the left to activate the position function.
Now you can click on any element of the plot to see its coordinates.

The coordinates of the selected point are displayed in red at the bottom-right part of the plot.
Click again on the first button of the pop-up bar to hide the coordinates from the plot.



Settings of plots
#################################
For each plot you can set the axes range and the thickness of the lines from the *Plot settings* page.
You can access the settings by clicking on the *Plot Settings* button of a plot page
(for example see Fig. :ref:`Plot of residual strain` - h for the strain plot).

Export the plots as pictures
#################################
You can download to your computer all the plots as pictures.

By moving your mouse on the top-right side of each plot, a pop-up bar will be displayed 
(see for example Fig. :ref:`Fig plot the isomekes` - i).
Click on the second button from the left to download the picture.

Available formats are:

* PNG
* JPEG
* TIFF
* PDF (this is a vector graphic format that can be further edited with common graphic programs)


Export and import project files
*********************************
Project files, that contain all of the data from all of the analyses (elastic properties, strain given as input, 
covariance matrices, calculation parameters, results, notes etc.) are stored in a database-like format and can be exported
from the *File -> Export Project* menu in the top bar of EntraPT.

.. Attention::
	To preserve the privacy of the users and the complete control of their data, all of the data are deleted from 
	the server when the EntraPT session is terminated and cannot be recovered even by the server administrators. 
	Therefore, downloading the project to the users computer is the only way to have access to the data after the EntraPT session is terminated. 
	The upload and the download of the data to and from the server is always performed over a secure SSL connection.

By default, the project is always saved to a binary file with \*.ept extension.
A log file with a summary of the operations performed during the active session is also generated.
The project file and the log file are included in a compressed folder, with \*.zip extension that 
can be downloaded to the computer of the user.


During the export procedure, the user can also choose to save the data to spreadsheets (with \*.xlsx extension) 
that can be read by any commonly used spreadsheet application, such as Microsoft Excel® or LibreOffice®. 
In this case an individual spreadsheet is created for each analysis.
Depending on the number of analyses in the current project, this  procedure can take a few minutes.
The generated spreadsheets are included in the compressed folder together with the project and the log files


The binary project files with \*.ept extension can be imported back into EntraPT, 
from the *File -> Import Project* menu in the top bar.
Importing a project file puts EntraPT in the same configuration as when the project file was created.
Once a project is loaded, you can visualize the data and make new plots.
You can also add new analyses or delete existing analyses.
Multiple project files can be merged in EntraPT to create larger datasets.
Such project files can be easily shared, making the checking and the comparison of data and results reliable. 
The project file can be easily imported into MATLAB® and custom scripts can be developed to further process the data
(:ref:`see an example here<Export project: save data to your computer and further processing>`). 



Change the P and T scales
**************************
The pressure and temperature scales can be changed from the *Settings* menu in the top bar of EntraPT.

At the beginning of a new session, by default the T scale is set to Celsius and the P scale to GPa.
The user can change them to any combination of the following units:

* Temperature

	* Kelvin
	
	* Celsius
	
* Pressure

	* GPa
	
	* kbar