What is EntraPT
==================

EntraPT is a web-based application for elastic geobarometry and it is freely accessible upon registration
at the website of the `“Fiorenzo Mazzi” Experimental Mineralogy Lab <https://www.mineralogylab.com/software/entrapt>`_.

It provides an easy-to-use tool to calculate the entrapment conditions of inclusions, with error propagation, from the residual strain measured in mineral inclusions.
EntraPT establishes a method and a workflow to import and analyze the measured residual strains, calculate the mean stress in the inclusions, compute the entrapment isomekes with uncertainty estimation, and visualizes all the results in relevant graphs.
It enables the user to avoid the many possible errors that can arise from manual handling of the data and from the numerous steps required in geobarometry calculations.

All of the data, parameters, and settings are stored in a consistent format and can be exported as project files and spreadsheets, and imported back to EntraPT for further analysis.
This allows researchers to store and/or share their data easily, making the checking and the comparison of data and results reliable.



EntraPT is a platform under constant update: new functionalities are made available from advances in the development of elastic geobarometry.



Supported browsers
*******************
.. _supported browsers:

EntraPT is supported on these browsers: Google Chrome™, Safari®, Firefox®, Microsoft Edge®.

Some browsers may prompt a warning regarding the security of the certificates, which you have to accept to see EntraPT.
Do not worry, your data are secure! EntraPT uses a secure SSL connection, and it will not ask for personal data apart from your name and email address upon registration. 

If you still cannot load the web app, delete the chronology of your browser including all the cookies and refresh the page.

If this does not solve the issue, try a different browser. If you cannot open EntraPT still, 
please `contact <software@mineralogylab.com>`_ us specifying the operating system of your computer,  the web browser you are using and its version number.


Development
*******************

The app is developed with MATLAB® AppDesigner and deployed with MATLAB® Compiler on Web App Server.

For all of the calculations based on the equations of state (EoS) of the minerals
(e.g. the calculation of the residual pressure and of the entrapment isomeke), EntraPT relies on Eosfit7
a stable and efficient Fortran code that has been validated over many years.
Eosfit7 is freely available at at `Ross Angel's website <http://www.rossangel.com/home.htm>`_ and is described in :cite:t:`Angel2014eosfit`.
In particular EntraPT implements an API built around Eosfit7n, a fast version of the program specifically designed to be called as a service within Matlab.

