A bit of theory
==================
The aim of this section is to give a short and to-the-point introduction to elastic thermobarometry (with a focus on Raman barometry), 
and to present the procedures implemented in EntraPT to calculate
the residual stress, the pressure and the isomeke and to propagate the uncertainties.

Several articles published over the years have presented the theory in far more details. The appropriate literature is cited in the following discussion,
and we suggest that the user refers to it in order to gain more insight on the assumptions, the potentialities an the limitations of elastic thermobarometry.


Elastic thermobarometry
*******************************
A residual strain is developed in the inclusion during 
exhumation of a host-inclusion pair to the Earth’s surface, because of the contrast in elastic properties 
between the two minerals. The inclusion does not expand in response to changes in P and T as would a free crystal.
Instead it is restricted by the host mineral, and this confinement can result in a residual strain, whose magnitude depends
on the entrapment conditions and on the elastic properties of the mineral pair.

If the residual strain is interpreted correctly, 
the conditions (i.e. the pressure or the temperature) at entrapment, or in case of viscous relaxation the conditions of the mechanical reset,
can be estimated through elastic barometry by using the elastic properties of the host and the inclusion.
The basic concept has been known for long time
(e.g. :cite:t:`Rosenfeld1961, Zhang1998, Guiraud2006, enami2007, angel2017eosfit`. See also :cite:t:`moulas_calculating_2020` for a comparison of the models proposed over time).
This theory assumes that the host and the inclusion are elastically isotropic.
:cite:t:`angel2017eosfit` have formulated this problem in a way that can include non-linear PVT equations of state of minerals, and this is the approach that is also implemented in EntraPT.

The entrapment (or reset) conditions are calculated as the hydrostatic conditions under which there are no stress gradients across the host and inclusion. 
All the possible entrapment conditions for a specific host-inclusion system lie on the entrapment isomeke, a line in P-T space along which, 
in isotropic systems, the strain and the stress are uniform in the system.
See :cite:t:`Angel2014relaxation, Angel2015JMG` for a detailed description of the thermodynamic basis of the isomeke and its significance.

The initial information for any calculation is always the knowledge of the residual strain (or stress) in the inclusion. 
It can be obtained from  measurements with Raman spectroscopy or single-crystal X-ray diffraction.
Once this is known, the determination of the entrapment isomeke requires the PVT equations of state (PVT EoS) of the host and inclusion.


.. note::
   EntraPT assumes a purely elastic behaviour of the host-inclusion system upon exhumation. Therefore viscous and plastic relaxation
   are not accounted for in the calculation.
   
..  
   See the section `Non-elastic relaxation`_ for more details.
   
   
Residual strain and stress
*******************************
.. _Residual strain and stress:

Since Raman spectrometers are usually readily available in Geosciences laboratories, Raman spectroscopy is a common choice to determine the residual strain of inclusions by measuring the Raman shifts of several modes.

Often, hydrostatic calibrations are used to convert the Raman shifts measured in the inclusion into a pressure. This is a valid approximation for crystals under hydrostatic conditions (e.g. :cite:t:`Schmidt2000`).
However, no mineral is elastically isotropic, and no inclusion is a perfect isolated sphere. As a consequence the stress field inside an inclusion is not hydrostatic when the host is in the 
laboratory. See for example :cite:t:`Mazzucchelli2018, murri_raman_2018, Mazzucchelli2019, zhong_analytical_2019, zhong_analytical_2021` for a discussion on why the strain 
and stress in inclusions are in general not isotropic. 
The shifts of Raman lines of the inclusion are therefore different from those under hydrostatic conditions.
Using hydrostatic calibrations of Raman shifts can lead to inaccurate estimates 
of the residual pressure in inclusion under non-hydrostatic stress and, in turn, of the entrapment conditions.
Moreover, it is often observed that when the stress in the inclusion is not hydrostatic but an hydrostatic calibration is used, different Raman modes will
give different residual pressures for the same inclusion (e.g. :cite:t:`bonazzi_assessment_2019`).

In order to determine the strain state of an inclusion crystal relative to a free crystal from the measured Raman shifts, the concept of mode Grueneisen tensor can be applied :cite:p:`Gruneisen1926`.
The papers of :cite:t:`murri_raman_2018, Angel2018Strainman` give a detailed introduction on this concept and on its application in the framework of Raman barometry.
The program stRAinMAN (freely available at `Ross Angel's website <http://www.rossangel.com/home.htm>`_) allows the user to obtain the strain components of the inclusion (and its uncertainties) from the 
measured Raman shifts. The theory and the algorithm behind this program are described in :cite:t:`Angel2018Strainman`.
EntraPT can import strains and their uncertainties as obtained from stRainMAN as text (\*.dat) files, in order to process simultaneously the data of many inclusions.

Another option is to determine the residual strain by direct in-situ X-ray diffraction (XRD) measurements of the inclusion (e.g. :cite:t:`Anzolini2019, Alvaro2020`).
In this case, measurements need to be carefully interpreted because irregular
and faceted inclusions may exhibit stress and strain gradients. In this case, diffraction measurements only provide some average value of strain over the whole
inclusion volume (see for example  :cite:t:`murri_raman_2018`).
However, with XRD the value of each strain component can be potentially determined independently,
and their uncertainties are estimated from the diffraction data.

How EntraPT calculates the residual stress and pressure from the strain
**************************************************************************

Once the strains of the inclusion measured at room temperature are known,
the residual stress in the inclusion can be determined by applying linear elasticity via the elastic tensor:

.. math:: \sigma_{i} = C_{ij} \varepsilon_j
	:label: stress

where :math:`C_{ij}` is the matrix representation in Voigt notation of the 4th rank elastic modulus tensor of the inclusion determined at room conditions. 

When the residual strain of the inclusion is measured, 
there are two possible definitions of residual pressure (:math:`P_{inc}`) for anisotropic inclusions:

1) The pressure is calculated as the negative of the mean normal stress:

.. math:: P_{inc}^{strain}=-(σ_1+σ_2+σ_3)/3
	:label: pinc_strain

2) An alternative approach is to first find the residual volume strain as the sum of the normal components of the strain:

.. math:: ε_V =ε_1+ε_2+ε_3
	:label: volume_strain

from which the residual pressure is obtained using the equation of state (EoS) of the inclusion as:

.. math:: P_{inc}^V  = f_{EoS} (ε_V)
	:label: pinc_volume

where :math:`f_{EoS}` represents the specific equation of the PVT EoS of the inclusion.

By default, EntraPT uses model (1) for the calculation of residual pressure.
However, the user can enable the Expert mode panel from the Settings menu and choose model (2) or both the models to explore a comparison of the results. 
 
 
The covariance matrix of the residual strain is used to propagate the uncertainties on the calculated residual pressures and the isomekes.
If model (1) is selected, the residual stress in the inclusion is calculated from the residual strain [see eq. :eq:`stress`] and the covariance matrix on the residual stress (:math:`V^σ`)  
is obtained from that on the residual strain (:math:`V^ε`) as:

.. math:: V^σ=C V^ε  C^T
	:label: cov_stress

Where :math:`C` is the matrix representation in Voigt notation of the 4th rank elastic modulus tensor of the inclusion, and :math:`C^T` its transpose.
Eq. :eq:`cov_stress` assumes that the uncertainties on the elastic components are negligible compared to the uncertainties of the measured strains.
The typical uncertainty on the elastic components is usually less than 2\% compared to a typical uncertainty
on the strain larger than 5\%. Moreover, the covariances of the elastic components determined experimentally are often not
reported in literature and cannot be used for the uncertainty propagation.

Once the residual stress and its covariance matrix are known, the standard deviation on the residual pressure :math:`P_{inc}^{strain}` are calculated by EntraPT as:

.. math::  esd(P_{inc}^{strain}) = (1/3) \sqrt{V_{11}^σ + V_{22}^σ + V_{33}^σ + 2V_{12}^σ + 2V_{13}^σ  + 2V_{23}^σ }	
	:label: esd_pinc_strain

On the other hand, if model (2) is selected, the uncertainty on the strain components is propagated by EntraPT into
the estimated standard deviation of the volume strain :math:`esd(ε_V )` as:

.. math::  esd(ε_V) = \sqrt{V_{11}^ε+V_{22}^ε+V_{33}^ε +2V_{12}^ε+2V_{13}^ε  +2V_{23}^ε}
	:label: esd_pinc_volumestrain
	
Given this uncertainty on the volume strain the standard deviation on the residual pressure :math:`P_{inc}^V` is calculated as:

.. math:: esd(P_{inc}^V )=(1/2) \left( \lvert f_{EoS}(ε_V+esd(ε_V )) - f_{EoS}(ε_V )\rvert + |f_{EoS}(ε_V - esd(ε_V ))-f_{EoS} (ε_V )|  \right)
	:label: esd_pinc_volume




The isomeke
***************


The calculation of the possible entrapment conditions for the host-inclusion pair is performed assuming isotropic elasticity 
with the model proposed by :cite:t:`angel2017eosfit`, using the PVT equations of state of the host and the inclusion.

The Pend and Tend are the pressure and temperature at which residual strain is measured and are assumed to be room conditions (25 °C or 298 K and 0 GPa).
Since the calculation of the residual pressure with eq. :eq:`pinc_strain` requires the elastic tensors of each inclusion, which are mostly only known for ambient P,T, 
EntraPT is necessarily restricted to calculations from strain measured when the host is at room conditions.

The uncertainties on the entrapment isomeke are estimated from the standard deviation on the residual pressure :math:`esd(P_{inc})`. The extreme values of the residual pressure are 
evaluated assuming an uncertainty on the residual pressure equivalent to one standard deviation as:

.. math::

	P_{inc}^{max} = P_{inc} + esd(P_{inc})
	
	P_{inc}^{min} = P_{inc} - esd(P_{inc})	

For each temperature step :math:`T_{iso}` along the isomeke, 
the :math:`P_{iso}` is the central value of the pressure on the isomeke, and is computed starting from the residual pressure :math:`P_{inc}`.
The :math:`P_{iso}^{max}` and :math:`P_{iso}^{min}` are the boundaries on the uncertainty on the isomeke, and are calculated
from the :math:`P_{inc}^{max}` and :math:`P_{inc}^{min}`, respectively.
For each :math:`T_{iso}` on the isomeke the corresponding uncertainty on the pressure of the isomeke :math:`P_{iso}` is obtained as:

.. math:: esd(P_{iso})=1/2 (|P_{iso}^{max} - P_{iso} |+ |P_{iso}^{min} - P_{iso}|)


..

	Non-elastic relaxation
	******************************
	.. _Non-elastic relaxation: