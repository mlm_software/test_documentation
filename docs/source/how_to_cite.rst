How to cite
==================

If you use results obtained from EntraPT, or if you use the program to process your data please cite it as:

* Mazzucchelli, M. L., Angel, R. J., & Alvaro, M. (2021). EntraPT: An online platform for elastic geothermobarometry. American Mineralogist, 106(5), 830–837. https://doi.org/10.2138/am-2021-7693CCBYNCND



Please consider also to cite the appropriate methodology papers to support further developments:

* Angel, R. J., Mazzucchelli, M. L., Alvaro, M., Nimis, P., & Nestola, F. (2014). Geobarometry from host-inclusion systems: The role of elastic relaxation. American Mineralogist, 99(10), 2146–2149. https://doi.org/10.2138/am-2014-5047
* Angel, R. J., Nimis, P., Mazzucchelli, M. L., Alvaro, M., & Nestola, F. (2015). How large are departures from lithostatic pressure? Constraints from host–inclusion elasticity. Journal of Metamorphic Geology, 33(8), 801–813. https://doi.org/10.1111/jmg.12138
* Mazzucchelli, M. L., Burnley, P., Angel, R. J., Morganti, S., Domeneghetti, M. C. C., Nestola, F., & Alvaro, M. (2018). Elastic geothermobarometry: Corrections for the geometry of the host-inclusion system. Geology, 46(3), 231–234. https://doi.org/10.1130/G39807.1
* Mazzucchelli, M. L., Reali, A., Morganti, S., Angel, R. J., & M, A. (2019). Elastic geobarometry for anisotropic inclusions in cubic hosts. Lithos, 105218. https://doi.org/10.1016/j.lithos.2019.105218


