Change log
=====================
* | EntraPT v1.4.0 (online since 25 July 2022)
  | More details are now reported in the log during the calculation of the entrapment isomeke.
  
* | EntraPT v1.3.0 (online since 10 May 2022)
  | New elastic properties for garnets available after :cite:t:`angel_garnet_2022`. See :ref:`here what has changed<Garnet (new EoS)>`.

* | EntraPT v1.2.0 (online since 13 April 2021)
  | Fixes to user registration issues and improved logon speed

* | EntraPT v1.1.0 (online since 27 November 2020)
  | Fixes to improve stability

* | EntraPT v1.0.0 (online since 11 June 2020)
  | First public release