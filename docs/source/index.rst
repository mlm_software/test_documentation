Welcome to EntraPT's documentation!
===================================


Do you want to apply Raman thermobarometry?
Have you measured the strain states of your inclusions and you want to visualize the results and estimate their entrapment conditions?

EntraPT is a new web-application for elastic geobarometry, designed to easily calculate the entrapment conditions of inclusions, with error propagation, from the measured residual strain.
You can import and analyze the measured residual strains, calculate the mean stress in the inclusions, compute the entrapment isomekes with uncertainty estimation, and directly visualize all the results in plots.
It also makes easy to export, reuse, share and compare your data. 

EntraPT is freely accessible upon registration at the website of the “Fiorenzo Mazzi” Experimental Mineralogy Lab. Click `here <https://www.mineralogylab.com/software/entrapt>`_ to start using EntraPT.

User guide
***********************
.. toctree::
   :maxdepth: 1
   
   introduction
   theory
   how_to_use_entrapt
   tutorial

 
Elastic properties
*************************************  

.. toctree::
   :maxdepth: 1
   
   elastic_properties
..  conventions
   

   
Support
****************************  
.. toctree::
   :maxdepth: 1
    

   report_bug
   changelog
   contact_us


Acknowledgments
****************************  
.. toctree::
   :maxdepth: 1
   
   how_to_cite
   acknowledgments
..  external_libraries
   
   
References
**************************** 
.. toctree::
   :maxdepth: 1
   
   references



